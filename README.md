# README #

This folder is for sharing Open Rights Group Birmingham resources

### Who do I talk to? ###

* Andy Wootton
* Other community or team contact
Twitter: @OpenRightsBrum

The graphics were created in line with a plan to take the original ORG logo and revolve it, changing the binary digits to say on each 2-bit line respectively, "0121", the Birmingham telephone area code.